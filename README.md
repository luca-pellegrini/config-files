# Config files

I miei file di configurazione personali, per le distribuzioni GNU/Linux che uso.

## Struttura delle cartelle

* `common`: configurazioni comuni a tutte le combinazioni di distro e desktop environment che uso (Linux Mint + Cinnamon, Debian + KDE Plasma, etc.).
  * `etc`:  configurazioni a livello di sistema (corrisponde a `/etc` sul sistema installato)
  * `HOME`: configurazioni a livello di utente. Le sottocartelle di questa cartella rappresentano altrettante categorie tematiche in cui sono raggruppati i file per agevolarne la manutenzione. Dentro ciascuna di queste sottocartelle, i file sono organizzati secondo la stessa gerarchia che avrebbero nel sistema installato, ad es. i file dentro alla sottocartella `bash` vanno copiati nella cartella _home_ nell'utente, e non in una sottocartella `~/bash`.
  * `usr`:  altri file di configurazione e risorse a livello di sistema
* `debian`:    configurazioni specifiche per Debian. La struttura delle sottocartelle segue la stessa logica illustrata sopra
* `linuxmint`: configurazioni specifiche per Linux Mint.
* `fedora`:    configurazioni specifiche per Fedora Linux
* `cinnamon`:  configurazioni specifiche per sistemi con desktop environment _Cinnamon_
* `kde`:       configurazioni specifiche per sistemi con desktop environment _KDE Plasma_

Ciascuna cartella di primo livello (`common`, `debian`, `fedora`, e simili) va considerata come la radice del filesystem per i file e le sottocartelle in essa contenuti (con l'eccezione della sottocartella `HOME` sopra illustrata).

Precedentemente, questo repository era diviso in altri due rami ('linuxmint' e 'fedora-kde'), dedicati, rispettivamente, a Linux Mint con DE Cinnamon e a Fedora con DE KDE Plasma. Tuttavia, ho è deciso di unificare i rami e adottare invece la struttura sopra descritta.

## Setup

Dipendenze: `git` e `stow` devono essere installati:

```bash
sudo apt install git stow
```

Eseguire nel terminale:

```bash
git clone https://git.disroot.org/luca-pellegrini/config-files.git $HOME/config-files
cd $HOME/config-files && chmod +x setup.sh && ./setup.sh
```

### setup.sh

Questo script esegue in automatico la configurazione dei file necessari, copiandoli o creando symlink che dalla cartella $HOME (e appropriate sottocartelle) puntano ai file della copia in locale di questo repository.

In particolare:
 * crea un simlink per tutti i file delle cartelle `bash` e `zsh` nella $HOME
 * crea una copia dei file `*.desktop` della cartella `desktop-files/.local/share/applications/` in $HOME/.local/share/applications/
