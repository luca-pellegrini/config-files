#!/usr/bin/bash
# ~/.bash_aliases: Alias definitions
# Questo file viene letto da ogni shell bash interattiva, dopo il file '~/.bashrc'

# Common commands
alias q='exit'
alias c='clear'
alias h='history'
alias cs='clear;ls'
alias p='cat'
alias o='xdg-open' # open any file in the configured default application
#alias t='time'
#alias k='kill'

# exa/ls aliases
alias ls="exa --icons --color=auto --group-directories-first"
alias la="ls --all"
alias ll="ls --all --long --group --header"
alias tree="ls --all --tree -L 3"

# Colorize grep output (good for log files)
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias fgrep="fgrep --color=auto"

# confirm before overwriting something
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"

# Directories
alias home="cd ~"
alias data="cd /Data"
alias root="cd /"
alias ..="cd .."
alias ...="cd ..; cd .."
alias ....="cd ..; cd ..; cd .."
# Directories in home and Data partition
alias libri="cd $HOME/Libri"
alias dendron-git="cd /Data/Dendron && git status"
alias nextcloud="cd /Data/Nextcloud && la"
alias python-lab="cd /Data/Git/python-lab && la"
alias python-appunti="cd /Data/Programmazione/python-appunti && la"
alias programmazione="cd /Data/Programmazione && la"

# Git
alias g="git"
alias add="git add"
#alias stat="git status"
alias commit="git commit"
alias pull="git pull"
alias push="git push"

# apt
alias upgrade="sudo freshclam; sudo apt update && sudo apt upgrade"
alias apt-info="apt show"

# Altri programmi usati di frequente
alias python="python3"
alias dolphin="dolphin --new-window"
alias dendron="/usr/bin/codium --new-window /Data/Dendron/dendron.code-workspace"
alias email="thunderbird"

# Custom neofetch
alias neofetch="neofetch --source /home/luca/rog_ascii_logo.txt"
