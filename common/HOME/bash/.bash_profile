# shellcheck shell=bash
# ~/.bash_profile: executed by the command interpreter for login shells.
#
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	    #shellcheck disable=SC1091
        . "$HOME/.bashrc"
    fi
fi

# Environment variables
# set PATH so it includes 'sbin' directories
PATH="$PATH:/usr/local/sbin:/usr/sbin:/sbin"

# set PATH so it includes '~/AppImage' directory, if it exists
if [ -d "$HOME/AppImage" ]; then
    PATH="$HOME/AppImage:$PATH"
fi

# set PATH so it includes user's private bin, if it exists
if [ -d "$HOME/.local/bin" ]; then
    PATH="$HOME/.local/bin:$PATH"
fi
if [ -d "$HOME/bin" ]; then
    PATH="$HOME/bin:$PATH"
fi

# XDG Base Directory specification
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

# XDG Desktop Portal
#if [[ "$XDG_CURRENT_DESKTOP" == "KDE" ]]; then
    #export GTK_USE_PORTAL=1  # Make GTK apps use KDE's file chooser
#fi

if [[ -n "$DISPLAY" || -n "$WAYLAND_DISPLAY" ]]; then
    export BROWSER=firefox
else
    export BROWSER=
fi

# Start ssh-agent in the background
eval "$(ssh-agent -s)" > /dev/null
