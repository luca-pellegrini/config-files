#!/bin/zsh
# .zshrc

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Zap plugin manager
# Source '~/.local/share/zap/zap.zsh' if it exists
if [ -f "$HOME/.local/share/zap/zap.zsh" ]; then
	source "$HOME/.local/share/zap/zap.zsh"
fi

# History
HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=2000

# Export some variables
export EDITOR="xed"
export TERMINAL="gnome-terminal"
export BROWSER="firefox"

# Alias definitions
# Must be sourced before the zsh plugins
if [ -f "$HOME/.zsh_aliases" ]; then
    source "$HOME/.zsh_aliases"
fi

# Plugins (Zap plugin manager)
plug "zsh-users/zsh-autosuggestions"
plug "zsh-users/zsh-syntax-highlighting"
#plug "zap-zsh/supercharge"
#plug "zap-zsh/exa"
# Custom prompt
plug "romkatv/powerlevel10k"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
[ -f "$HOME/.local/share/zap/zap.zsh" ] && source "$HOME/.local/share/zap/zap.zsh"
